from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib import auth
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from jobapp.models import *
from ckeditor.widgets import CKEditorWidget


    

class JobForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['title'].label = "Название работы :"
        self.fields['location'].label = "Местоположение :"
        self.fields['salary'].label = "Оплата :"
        self.fields['description'].label = "Описание работы :"
        self.fields['last_date'].label = "Дата актуальности работы :"
        self.fields['company_name'].label = "Работодатель :"
        self.fields['url'].label = "Контактные данные :"


        self.fields['title'].widget.attrs.update(
            {
                'placeholder': 'напр: Сантехник',
            }
        )        
        self.fields['location'].widget.attrs.update(
            {
                'placeholder': 'напр: Астана',
            }
        )
        self.fields['salary'].widget.attrs.update(
            {
                'placeholder': 'напр: 5000 тенге',
            }
        )                    
        self.fields['last_date'].widget.attrs.update(
            {
                'placeholder': 'YYYY-MM-DD ',
                
            }
        )        
        self.fields['company_name'].widget.attrs.update(
            {
                'placeholder': 'Работодатель',
            }
        )           
        self.fields['url'].widget.attrs.update(
            {
                'placeholder': 'Веб-сайт или почта',
            }
        )    


    class Meta:
        model = Job

        fields = [
            "title",
            "location",
            "job_type",
            "category",
            "salary",
            "description",
            "last_date",
            "company_name",
            "url"
            ]
        widgets = {
            'url': forms.TextInput(attrs={'placeholder': 'Веб-сайт или телефон', 'required': False}),  # Установите required в False
        }
    

    def clean_job_type(self):
        job_type = self.cleaned_data.get('job_type')

        if not job_type:
            raise forms.ValidationError("Service is required")
        return job_type
    
    def clean_category(self):
        category = self.cleaned_data.get('category')

        if not category:
            raise forms.ValidationError("category is required")
        return category


    def save(self, commit=True):
        job = super(JobForm, self).save(commit=False)
        if commit:
            
            job.save()
        return job




class JobApplyForm(forms.ModelForm):
    class Meta:
        model = Applicant
        fields = ['job']

class JobBookmarkForm(forms.ModelForm):
    class Meta:
        model = BookmarkJob
        fields = ['job']




class JobEditForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['title'].label = "Название работы :"
        self.fields['location'].label = "Местоположение :"
        self.fields['salary'].label = "Оплата :"
        self.fields['description'].label = "Описание работы :"
        self.fields['last_date'].label = "Дата актуальности работы :"
        self.fields['company_name'].label = "Работодатель :"
        self.fields['url'].label = "Контактные данные :"


        self.fields['title'].widget.attrs.update(
            {
                'placeholder': 'напр: Сантехник',
            }
        )        
        self.fields['location'].widget.attrs.update(
            {
                'placeholder': 'напр: Bangladesh',
            }
        )
        self.fields['salary'].widget.attrs.update(
            {
                'placeholder': 'напр: 5000 тенге',
            }
        )
        # self.fields['tags'].widget.attrs.update(
        #     {
        #         'placeholder': 'Use comma separated. eg: Python, JavaScript ',
        #     }
        # )                        
        self.fields['last_date'].widget.attrs.update(
            {
                'placeholder': 'YYYY-MM-DD ',
            }
        )        
        self.fields['company_name'].widget.attrs.update(
            {
                'placeholder': 'Работодатель',
            }
        )           
        self.fields['url'].widget.attrs.update(
            {
                'placeholder': 'Почта или телефон',
            }
        )    

    
        last_date = forms.CharField(widget=forms.TextInput(attrs={
                    'placeholder': 'Service Name',
                    'class' : 'datetimepicker1'
                }))

    class Meta:
        model = Job

        fields = [
            "title",
            "location",
            "job_type",
            'category',
            "salary",
            "description",
            "last_date",
            "company_name",
            "company_description",
            "url"
            ]
        
        widgets = {
            'url': forms.TextInput(attrs={'placeholder': 'Почта или телефон', 'required': False}),  # Установите required в False
        }


    def clean_job_type(self):
        job_type = self.cleaned_data.get('job_type')

        if not job_type:
            raise forms.ValidationError("Job Type is required")
        return job_type

    def clean_category(self):
        category = self.cleaned_data.get('category')

        if not category:
            raise forms.ValidationError("category is required")
        return category
    
    def save(self, commit=True):
        job = super(JobEditForm, self).save(commit=False)
      
        if commit:
            job.save()
        return job

